from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view
from rest_framework import status
import json
from tienda_m.models import Contacto

@api_view(['GET', 'POST'])
def contacto(request):
    if request.method == 'GET':
        response_json = []
        contactos = Contacto.objects.all()       
        for contacto in contactos:
            js = {
                'run': int(contacto.run),
                'dv' : contacto.dv,
                'nombres' : contacto.nombres,
                'apellido_paterno' : contacto.apellido_paterno,
                'apellido_materno' : contacto.apellido_materno,
                'email' : contacto.email,
                'telefono' : contacto.telefono,
                'asunto' : contacto.asunto                
            }
            response_json.append(js)
        status_code = status.HTTP_200_OK
        print('response_json', response_json)
        return HttpResponse(json.dumps(response_json, ensure_ascii=False), content_type="application/json", status=status_code)            
