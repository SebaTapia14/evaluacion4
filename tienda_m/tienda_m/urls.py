"""tienda_m URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path
from tienda_m.views.perruqueria import perruqueria
from tienda_m.views.producto import producto_perruqueria
from tienda_m.views.contacto import contacto_perruqueria, formulario_contacto
from tienda_m.views.mantenedorcontacto import load_contacto
from tienda_m.views.registro import registro_perruqueria, formulario_registro
from tienda_m.views.mantenedorregistro import load_registro
from tienda_m.views import login
from tienda_m.views import logout
from django.contrib.auth.models import Permission, ContentType
from tienda_m.models import Contacto
from tienda_m.views import errorpage
from tienda_m.api.v1 import apicontacto

admin.site.register(Permission)
admin.site.register(ContentType)
admin.site.register(Contacto)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('perruqueria/', perruqueria),
    path('', perruqueria),
    path('producto/', producto_perruqueria),
    path('contacto/', contacto_perruqueria),
    path('contacto/formulario', formulario_contacto),    
    path('mantenedor-contacto/', load_contacto),
    path('registro/', registro_perruqueria),
    path('registro/formulario', formulario_registro),    
    path('mantenedor-registro/', load_registro),
    path('login', login.perruqueria),  
    path('logout/', logout.logout_user),
    path('error-401/', errorpage.error_401_page),
    path('error-403/', errorpage.error_403_page),
    #API REST
    path('api/v1/contacto', apicontacto.contacto)

]


